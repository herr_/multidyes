#!/bin/sh

if [ $1 = "--help" ]; then
  echo "Usage: $0 <arguments>"
  echo "\t-u - check for updates"
  echo "\t-c - compile the source code"
  exit 0
fi

compile () {
  javac -classpath RSBot.jar src/*/*.java -d out/
}

#Download RSBot if it doesn't excist
if [ ! -f RSBot.jar ]; then
  echo "[INFO] Downloading RSBot.."
  wget --quiet -O RSBot.jar https://www.powerbot.org/download/?jar
  echo "\t|_ Done"
fi
#Check for RSBot updates and update if needed
if echo $1$2 | grep -Eq 'u'; then
  echo "[INFO] Checking for updates.."
  wget --quiet -O /tmp/RSBot.jar https://www.powerbot.org/download/?jar

  if [ ! "$(md5sum /tmp/RSBot.jar | sed "s/ .*//g")" = "$(md5sum RSBot.jar | sed "s/ .*//g")" ]; then
    echo "\t|_ RSBot updated"
    rm RSBot.jar
    mv /tmp/RSBot.jar .
  else
    echo "\t|_ You already have the newest version of RSBot available"
  fi
fi

#Check if the source code hasn't been compiled yet
if [ ! -d out ]; then
  echo "[INFO] Compiling.."
  mkdir out
  compile
  echo "\t|_ Done"
fi
#Recompile if we need to
if echo $1$2 | grep -Eq 'c'; then
 echo '[INFO] Recompiling..'
 compile
 echo "\t|_ Done"
fi

echo "[INFO] Launching RSBot.."
java -Xmx768m -XX:+UseG1GC -cp "out/:RSBot.jar" jagexappletviewer
