package tasks;

import API.AntiBan;
import API.Task;
import debug.Logger;
import org.powerbot.script.Condition;
import org.powerbot.script.Tile;
import org.powerbot.script.rt6.ClientContext;
import script.Config;
import script.State;

import java.util.Random;
import java.util.concurrent.Callable;

public class WalkToBank extends Task<ClientContext> {

    private State state;
    private AntiBan antiban;
    private Config config;
    private Random random;

    private int chanceToTurnBeforeClicking;

    public WalkToBank(ClientContext ctx, State state, AntiBan antiban, Config config) {
        super(ctx);

        this.state = state;
        this.antiban = antiban;
        this.config = config;
        this.random = new Random();

        this.chanceToTurnBeforeClicking = this.random.nextInt(4) + this.random.nextInt(this.random.nextInt(6)) + 5;
                Logger.log("[INIT] chanceToTurnBeforeClicking = " + chanceToTurnBeforeClicking + " @WalkToBank.java");
    }

    @Override
    public boolean activate() {
        //If we're inside Aggie's house AND (If we're out of resources OR if backpack's full) AND Aggie's door's open
        return (state.areWeInsideAggiesHouse() && ((this.config.howManyDyesCanWeMake() == 0) || (ctx.backpack.moneyPouchCount() < 5)) && state.isAggiesDoorOpen());
    }

    @Override
    public void execute() {
        boolean camera_turned = false; //Has camera been turned already?

        //Turn camera before clicking bank, depending on chance
        if(this.random.nextInt(this.chanceToTurnBeforeClicking) == 0) {
            antiban.turnToWest();
            camera_turned = true;
        }

        if(areWeStuck())
            return;

        //Function excepts that we are in the house (or in the front yard) when we call it
        Tile banktile = AntiBan.getTileToBank(ctx);
        Logger.log("Walking to bank Tile: " + banktile.toString());

        //Get random bank tile (that is in the area we can go [Not too far away from us])
        ctx.movement.step(banktile);

        //If we are too far and have clicked the wrong tile because of it
        if(ctx.movement.destination().compareTo(banktile) != 0) {
            //click until we have clicked one of the bank tiles
            Condition.wait(new Callable<Boolean>() {
                @Override
                public Boolean call() {
                    if(areWeStuck() || state.isDestinationAtBank())
                        return true;

                    //Click again some banktile
                    ctx.movement.step(AntiBan.getTileToBank(ctx));

                    return false;
                }
            }, 600, 10);

            if(areWeStuck())
                return;

        }

        //If camera hasn't been turned yet, turn it to right direction
        if(!camera_turned)
            this.antiban.turnToWest();

        //Check last time if we're stuck
        if(!this.state.isAggiesDoorOpen() && this.state.areWeInsideAggiesHouse())
            return;

        //Wait until we are in bank
        Condition.wait(new Callable<Boolean>() {
            @Override
            public Boolean call() {
                return (ctx.players.local().tile().y() <= 3249);
            }
        }, 125, 80);

    }

    //Are we inside Aggie's house and is her door closed
    private boolean areWeStuck() {
        return (!this.state.isAggiesDoorOpen() && this.state.areWeInsideAggiesHouse());
    }

    @Override
    public String name() {
        return "Walking to the bank";
    }

}
