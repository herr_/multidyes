package tasks;

import API.MakeDye;
import API.Task;
import debug.Logger;
import org.powerbot.script.rt6.ClientContext;
import org.powerbot.script.rt6.Component;
import org.powerbot.script.rt6.Widget;
import resources.ID;
import script.Config;
import script.State;


public class MakeDyes extends Task<ClientContext> {

    private MakeDye makeDye;


    public MakeDyes(ClientContext ctx, State state, Config config) {
        super(ctx);

        //Set here the right MakeDye object depending on the config
        if(config.getDyeId() == ID.ID_BLUE_DYE)
            this.makeDye = new MakeDye(ctx, state, new Component(ctx, new Widget(ctx, 1188), 14), "2", config);
        else if(config.getDyeId() == ID.ID_RED_DYE)
            this.makeDye = new MakeDye(ctx, state, new Component(ctx, new Widget(ctx, 1188), 6), "1", config);
        else if(config.getDyeId() == ID.ID_YELLOW_DYE)
            this.makeDye = new MakeDye(ctx, state, new Component(ctx, new Widget(ctx, 1188), 35), "3", config);
        else
            Logger.log("[FATAL ERROR] in MultiDyes.java, will soon crash due to an null-ptr dereference..!");
    }

    @Override
    public boolean activate() {
        return this.makeDye.activate();
    }

    @Override
    public void execute() {
        this.makeDye.execute();
    }

    @Override
    public String name() {
        return this.makeDye.name();
    }

    public int getDyeCounter() {
        return this.makeDye.getDyeCounter();
    }

}