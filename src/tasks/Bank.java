package tasks;

import API.Task;
import debug.Logger;
import org.powerbot.script.rt6.ClientContext;
import resources.ID;
import script.Config;
import script.State;


public class Bank extends Task<ClientContext> {

    private API.Bank bank;

    private static int COST_DYE = 5;


    public Bank(ClientContext ctx, State state, final Config config, int withdrawAmount) {
        super(ctx);

        if(config.getDyeId() == ID.ID_BLUE_DYE) {
            this.bank = new API.Bank(ctx, config, state, (27 * COST_DYE), withdrawAmount) {

                @Override
                protected boolean doWeNeedToDeposit() {
                    return ((ctx.backpack.select().count() - ctx.backpack.select().id(ID.ID_WOAD_LEAF).count()) > 0);
                }

                @Override
                protected void deposit() {
                    if((ctx.backpack.select().count() - ctx.backpack.select().id(ID.ID_WOAD_LEAF).count()) > 0)
                        ctx.bank.depositAllExcept(ID.ID_WOAD_LEAF);
                    else
                        ctx.bank.depositInventory();
                }

            };
        } else if(config.getDyeId() == ID.ID_RED_DYE) {
            this.bank = new API.Bank(ctx, config, state, (9 * COST_DYE), withdrawAmount) {

                @Override
                protected void withdrawResources() {
                    if(ctx.bank.select().id(ID.ID_REDBERRIES).count() == 0)
                        return;
                    else if(ctx.bank.select().id(ID.ID_REDBERRIES).peek().stackSize() < 27)
                        ctx.bank.withdraw(ID.ID_REDBERRIES, org.powerbot.script.rt6.Bank.Amount.ALL);
                    else
                        ctx.bank.withdraw(ID.ID_REDBERRIES, org.powerbot.script.rt6.Bank.Amount.ALL_BUT_ONE);
                }

            };
        } else if(config.getDyeId() == ID.ID_YELLOW_DYE) {
            this.bank = new API.Bank(ctx, config, state, (14 * COST_DYE), withdrawAmount) {

                @Override
                protected void withdrawResources() {
                    ctx.bank.withdraw(ID.ID_ONION, org.powerbot.script.rt6.Bank.Amount.ALL);
                }

            };
        }else
            Logger.fatal("[FATAL ERROR] In tasks/Bank.java: No dye were selected! Will exit now.", ctx);

    }

    @Override
    public boolean activate() {
        return this.bank.activate();
    }

    @Override
    public void execute() {
        this.bank.execute();
    }

    @Override
    public String name() {
        return this.bank.name();
    }

}
