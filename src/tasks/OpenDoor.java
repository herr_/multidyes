package tasks;

import API.AntiBan;
import API.Door;
import API.Task;
import debug.Logger;
import org.powerbot.script.rt6.ClientContext;
import script.Config;
import script.State;

import java.util.Random;

public class OpenDoor extends Task<ClientContext> {

    private final Door doorToOpen;
    private final State state;
    private final Config config;

    private final Random random;


    public OpenDoor(ClientContext ctx, Door doorToOpen, State state, final Config config) {
        super(ctx);

        this.doorToOpen = doorToOpen;
        this.state = state;
        this.config = config;

        this.random = new Random();

    }

    @Override
    public boolean activate() {
        if (!this.doorToOpen.isDoorOpen()) { //Just in case (probably irrelevant)
            //We're going in
            if((this.state.areWeAtAggiesFrontYard() && (this.config.howManyDyesCanWeMake() >= 1)) || ((this.config.howManyDyesCanWeMake() >= 1) && this.doorToOpen.isDoorInViewPort())) //TODO: Test the latter
                return true;

            //We're leaving Aggie's house
            return (this.state.areWeInsideAggiesHouse() && ((this.config.howManyDyesCanWeMake() == 0) || (ctx.backpack.moneyPouchCount() < 5)));
        }

        return false;
    }

    @Override
    public void execute() {
        Logger.log("openDoor() called.");

        doorToOpen.openDoor();
    }

    @Override
    public String name() {
        return "Opening the door";
    }

}
