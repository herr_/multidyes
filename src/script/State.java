package script;

import API.Door;
import org.powerbot.script.Tile;
import org.powerbot.script.rt6.ClientContext;
import resources.Tiles;

import debug.Logger;

public class State {

    private final ClientContext ctx;
    private final Door aggiesDoor;

    //other variables
    private final static int COST_DYE = 5;

    public State(ClientContext ctx, Door aggiesDoor) {
        this.ctx =  ctx;

        this.aggiesDoor = aggiesDoor;
    }

    //are we in bank area
    public boolean areWeInTheBank() {
        Tile currentTile = ctx.players.local().tile();

        Logger.log("The current tile where the player is " + currentTile.toString());

        for(Tile t : Tiles.AREA_BANK) {
            if(t.compareTo(currentTile) == 0) {
                Logger.log("We are currently at bank-tile: " + t.toString());
                return true;
            }
        }

        return false;
    }

    //is the destination tile at the bank
    public boolean isDestinationAtBank() {

        //TILES_BANK
        for(Tile t : Tiles.TILES_BANK) {
            if(t.compareTo(ctx.movement.destination()) == 0)
                return true;
        }

        //AREA_BANK
        for(Tile t : Tiles.AREA_BANK) {
            if(t.compareTo(ctx.movement.destination()) == 0)
                return true;
        }

        return false;
    }

    //returns true if we are inside the house
    public boolean areWeInsideAggiesHouse() {
        for(Tile t : Tiles.AREA_AGGIES_HOUSE) {
            if (t.compareTo(ctx.players.local().tile()) == 0)
                return true;
        }

        return false;
    }

    //Is the destination ate Aggie's house
    public boolean isDestinationAtAggiesHouse() {
        for(Tile t : Tiles.AREA_AGGIES_HOUSE) {
            if(t.compareTo(ctx.movement.destination()) == 0)
                return true;
        }

        return false;
    }

    public boolean areWeAtAggiesFrontYard() {
        for(Tile t : Tiles.AREA_AGGIES_FRONTYARD) {
            if(t.compareTo(ctx.players.local().tile()) == 0)
                return true;
        }

        return false;
    }

    public boolean isDestinationAtAggiesFrontYard() {
        for(Tile t : Tiles.AREA_AGGIES_FRONTYARD) {
            if(t.compareTo(ctx.movement.destination()) == 0)
                return true;
        }

        return false;
    }

    public boolean isAggiesDoorOpen() {
        return aggiesDoor.isDoorOpen();
    }

}
