package script;

//TODO IMPLEMENT
//-Add config option to buy more leaves if we run out of them.
//-Maybe we should add new state TURN_CAMERA as the first Task (and some other antiban Tasks that activate semi/randomly)
//-Antiban: if the chat isn't displaying the newest stuff, scroll down! (Humans want to see what's happening)
//-Antiban: don't turn the camera always on same moment (e.g. do it sometimes before and sometimes after event x)
//-Antiban: close randomly last conversation widget
//-Make waiting time better (faster, more human like) and don't always rely on waiting thing x to finish if it isn't necessary
//-AntiBan: When we're talking with Aggie, we should sometimes spam click the dialog button (an extra click and very seldom few extra clicks)


//FIXME GENERAL
//-Do not create objects every time create once and reuse them
//-Do not use fixed values (When we shouldn't)
//-AntiBan: DON'T HARDCODE ANY RANDOMIZATION VALUES!

import API.AntiBan;
import API.Door;
import API.Task;
import debug.Logger;
import org.powerbot.script.PaintListener;
import org.powerbot.script.PollingScript;
import org.powerbot.script.Script;
import org.powerbot.script.rt6.ClientContext;
import resources.ID;
import resources.Tiles;
import tasks.*;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

@Script.Manifest(name = "MultiDyes", description = "Makes dyes in Draynor village for a nice profit.", properties = "author=vonribula; topic=1352558; client=6")


public class Main extends PollingScript<ClientContext> implements PaintListener {

    private script.State state;
    private Door aggiesDoor;
    private Config config;
    private AntiBan antiban;


    private Task[] taskList;
    private String currentState;

    private BufferedImage banner;

    @Override
    public void start() {

        //Config must be configured before creating any other objects
        config = new Config(ctx);

        //GUI for choosing the right dye
        String[] options = {"Blue dye", "Red dye", "Yellow dye"};
        String  choice = "" + (String) JOptionPane.showInputDialog(null, "Please select a dye below", "MultiDyes by vonribula", JOptionPane.PLAIN_MESSAGE, null, options, options[0]);

        if(choice.equals(options[0])) {
            this.config.setBlueDye();
            Logger.log("Blue dye selected.");
        }else if(choice.equals(options[1])) {
            this.config.setRedDye();
            Logger.log("Red dye selected.");
        }else if(choice.equals(options[2])) {
            this.config.setYellowDye();
            Logger.log("Yellow dye selected.");
        }else {
            Logger.fatal("[FATAL ERROR] No dye were selected! Will exit now.", ctx); //FIXME: Doesn't exit! (Exits only from poll() not from start()!)
        }

        aggiesDoor = new Door(ctx, Tiles.TILE_DOOR_OPEN, Tiles.TILE_DOOR_CLOSED);

        state = new script.State(ctx, aggiesDoor);
        antiban = new AntiBan(ctx);

        taskList = new Task[] {new WalkToAggie(ctx, aggiesDoor, config), new WalkToBank(ctx, state, antiban, config), new MakeDyes(ctx, state, config), new OpenDoor(ctx, aggiesDoor, state, config), new Bank(ctx, state, config, 10000)}; //TODO AntiBan the withdraw amount!

        //Download graphics
        //banner = downloadImage("https://i.imgur.com/gocl2JD.png");
        //banner = downloadImage("https://i.imgur.com/EuDZbZz.png");
        banner = downloadImage("https://i.imgur.com/ImVDJit.png");

    }

    @Override
    public void poll() {

        for(Task t : this.taskList) {
            if(t.activate()) {
                Logger.log("[STATE ACTIVATED]: " + t.name());
                this.currentState = t.name();
                t.execute();
                break;
            }
        }

    }

    //Make sure that we stop even if we're in the middle of a Task
    @Override
    public void stop() {
        ctx.controller.stop();
    }

    @Override
    public void repaint(Graphics graphics) {
        long ms = this.getTotalRuntime();
        long sec = (ms / 1000) % 60;
        long min = (ms / (1000 * 60)) % 60;
        long hrs = (ms / (1000 * 60 * 60)) % 24;

        Graphics2D g = (Graphics2D) graphics;
        int base_y = 0;
        int base_x = 0; //use these with offsets for easy modification of base

        int text_offset = 24;
        int image_base_y = 77; //above is added to this

        MakeDyes makeDyes = (MakeDyes) this.taskList[2];

        int dyes_h = (int) ((1.0 * makeDyes.getDyeCounter() * (1000 * 60 * 60) / ms)); //We need to convert counter to float so we don't overflow!

        //Draw cursor
        g.setColor(new Color(255, 255, 255));
        //g.drawRect((ctx.input.getLocation().x - 5), (ctx.input.getLocation().y - 5), 10, 10);
        //Draw the long lines
        g.drawLine(0, 0, (ctx.input.getLocation().x - 5), (ctx.input.getLocation().y - 5));
        g.drawLine(0, (int) ctx.game.getViewport().height, (ctx.input.getLocation().x - 5), (ctx.input.getLocation().y + 5));
        g.drawLine((int) ctx.game.getViewport().width, 0, (ctx.input.getLocation().x + 5), (ctx.input.getLocation().y - 5));
        g.drawLine((int) ctx.game.getViewport().width, (int) ctx.game.getViewport().height, (ctx.input.getLocation().x + 5), (ctx.input.getLocation().y + 5));
        //Draw the short lines
        g.drawLine(ctx.input.getLocation().x, (ctx.input.getLocation().y - 10), ctx.input.getLocation().x, (ctx.input.getLocation().y + 10));
        g.drawLine((ctx.input.getLocation().x - 10), ctx.input.getLocation().y, (ctx.input.getLocation().x + 10), ctx.input.getLocation().y);

        if(banner == null) {
        g.setColor(new Color(0, 0, 0, 192)); //rgb(a)
        g.fillRect(base_y, base_x, 150, 100); //y, x, w, h

        g.setColor(new Color(0, 0xbc, 0xa8));
        g.drawRect(base_y, base_x, 150, 100);//drawRect just draws the borders

        g.setColor(new Color(0x80, 0x00, 0xff));
        g.drawString("MultiDyes by vonribula", (base_y + 10), (base_x + 15));

        //Set the color according to the selected color
        if (this.config.getDyeId() == ID.ID_BLUE_DYE)
            g.setColor(new Color(0, 0, 255));
        else if (this.config.getDyeId() == ID.ID_RED_DYE)
            g.setColor(new Color(255, 0, 0));
        else if (this.config.getDyeId() == ID.ID_YELLOW_DYE)
            g.setColor(new Color(255, 255, 0));

        //Draw text in dye color
        g.drawString("Dyes (Dyes/h): " + makeDyes.getDyeCounter() + " (" + dyes_h + ")", (base_y + 10), (base_x + 30));

        //Reset the color
        g.setColor(new Color(0x80, 0x00, 0xff));

        g.drawString("Runtime: " + String.format("%02d:%02d:%02d", hrs, min, sec), (base_y + 10), (base_x + 45));

        g.drawString(this.currentState, (base_y + 10), (base_x + 75));

        }else {
            g.setColor(new Color(0xff, 0xff, 0xff));
            g.setFont(new Font("TimesRoman", Font.PLAIN, 16));
            //NEW GUI
            int y_offset = (int) ctx.game.getViewport().height - banner.getHeight();
            g.drawImage(banner, 0, y_offset, null);
            g.drawString(String.format("%02d:%02d:%02d", hrs, min, sec), 91, (image_base_y + y_offset));
            g.drawString(makeDyes.getDyeCounter() + " (" + dyes_h + ")", 111, (image_base_y + text_offset + y_offset));
            g.drawString("N/A", 119, (image_base_y + text_offset * 2 + y_offset));
            g.drawString(this.currentState, 72, (image_base_y + text_offset * 3 + y_offset));
        }

    }

}