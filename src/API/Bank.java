package API;

import debug.Logger;
import org.powerbot.script.Condition;
import org.powerbot.script.Random;
import org.powerbot.script.rt6.ClientContext;
import resources.ID;
import script.Config;
import script.State;

import java.util.concurrent.Callable;

public abstract class Bank extends Task<ClientContext> {

    private Config config;
    private State state;

    private int cashMinimum;
    private int withdrawAmount;

    private int chanceToCloseBankWindow;


    public Bank(ClientContext ctx, Config config, State state, int cashMinimum, int withdrawAmount) {
        super(ctx);

        this.config = config;
        this.state = state;

        this.cashMinimum = cashMinimum;
        this.withdrawAmount = withdrawAmount;

        this.chanceToCloseBankWindow = new java.util.Random().nextInt(5) + new java.util.Random().nextInt(3) + 4;
        Logger.log("[INIT] chanceToCloseBankWindow = " + chanceToCloseBankWindow);

    }

    @Override
    public boolean activate() {
        if ((this.state.areWeInTheBank() || ctx.objects.select().id(ID.ID_BANK_BOOTHS).nearest().peek().inViewport()) && ((this.config.howManyDyesCanWeMake() == 0) || (ctx.backpack.moneyPouchCount() < cashMinimum) || ((ctx.backpack.select().count() - ctx.backpack.select().id(this.config.getResourceId()).count()) > 0))); //TODO: Test
            return true;
    }

    @Override
    public void execute() {

        Logger.log("API/Bank.java - execute():");
        Logger.log("\tOpening bank interface.");
        //Open bank if it isn't already opened
        if(!ctx.bank.opened())
            AntiBan.openBank(ctx);

        Logger.log("\tWaiting for the interface to show up.");
        //Wait until the bank interface pops up
        Condition.wait(new Callable<Boolean>() {
            @Override
            public Boolean call() {
                if(ctx.bank.opened()) {
                    Logger.log("Bank interface opened!");
                    return true;
                }
                return false;
                //return ctx.bank.opened();
            }
        //}, Random.getDelay(), 40); //TODO set back to 20 if doesn't work with this
        }, Random.getDelay(), 20);

        if(!ctx.bank.opened())
            return;


        Logger.log("\tChecking whether we need to deposit stuff to bank:");
        //Deposit stuff if we need to
        if(doWeNeedToDeposit()) {
            Logger.log("\t\tWe need to deposit now stuff to bank!");
            final int items_before_deposit = ctx.backpack.select().count();
            deposit();

            Condition.wait(new Callable<Boolean>() {
                @Override
                public Boolean call() {
                    return (ctx.backpack.select().count() != items_before_deposit);
                }
            });
        }else //only for this debug message
                Logger.log("\t\tWe don't have to deposit anything for now.");


        Logger.log("\tChecking whether we need to withdraw money.");
        //Withdraw money if we need to and if we can
        if(ctx.backpack.moneyPouchCount() < cashMinimum) {
            //NOTE: If this if statement isn't checked in this order, this might crash (if we don't have item x in bank id().select() returns null!)
            if((ctx.bank.select().id(ID.ID_MONEY).count() == 0) || (ctx.bank.select().id(ID.ID_MONEY).peek().stackSize() < this.withdrawAmount))
                Logger.fatal("[FATAL ERROR]: Out of money! Will exit now.", ctx);
            else {
                final int money_before_withdraw = ctx.backpack.moneyPouchCount();
                ctx.bank.withdraw(ID.ID_MONEY, this.withdrawAmount);

                //Wait until we have withdrawn our money
                Condition.wait(new Callable<Boolean>() {
                    @Override
                    public Boolean call() {
                        return (ctx.backpack.moneyPouchCount() != money_before_withdraw);
                    }
                });

            }
        }

        Logger.log("\tChecking whether we can withdraw resources to our backpack.");
        if(resourcesInBank()) {
            Logger.log("\t\tWe can. Withdrawing resources..");
            withdrawResources();
        } else if(config.howManyDyesCanWeMake() == 0) {
            Logger.log("[DEBUG] Resources in the bank: " + ctx.bank.select().id(this.config.getResourceId()).count() + " and the resource per dye is: " + this.config.getResourcesPerDye());
            Logger.fatal("[FATAL ERROR]: Out of resources! Will exit now.", ctx);
        }


        Logger.log("\tWaiting");
        if(!(new java.util.Random().nextInt(this.chanceToCloseBankWindow) == 0)) {
            ctx.bank.close();

            Condition.wait(new Callable<Boolean>() {
                @Override
                public Boolean call() {
                    return !ctx.bank.opened();
                }
            }, Random.getDelay(), 20);
        }

    }

    @Override
    public String name() {
        return "Banking";
    }

    //This needs to be overwritten
    protected boolean doWeNeedToDeposit() {
        return (ctx.backpack.select().count() != 0);
    }

    //This needs to be overwritten
    protected void deposit() {
        ctx.bank.depositInventory();
    }

    private boolean resourcesInBank() {
        return (!((ctx.bank.select().id(this.config.getResourceId()).isEmpty()) || (ctx.bank.select().id(this.config.getResourceId()).peek().stackSize() < this.config.getResourcesPerDye())));
    }

    //This needs to be overwritten
    protected void withdrawResources() {
        ctx.bank.withdraw(this.config.getResourceId(), org.powerbot.script.rt6.Bank.Amount.ALL);
    }

}
