package API;

public abstract class Dye {

    //Returns the number of how many dyes we can make from resources
    public abstract int howManyDyesCanWeMake();

}
