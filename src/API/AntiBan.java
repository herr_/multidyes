package API;

import debug.Logger;
import org.powerbot.script.Tile;
import org.powerbot.script.rt6.ClientContext;
import org.powerbot.script.rt6.GameObject;
import org.powerbot.script.rt6.Npc;
import resources.ID;
import resources.Tiles;

import java.util.Random;

//TODO: Move here all the timing stuff and make getter functions

//TODO: Make this a class that initializes timing values in constructor

public class AntiBan {

    private final ClientContext ctx;

    private Random random;

    private int camera_radius_off_by_min; //TODO FIXME: This shouldn't be hardcoded!! //This value is added or removed from the camera jaw
    private int camera_radius_off_by_max;


    public AntiBan(ClientContext ctx) {
        this.ctx = ctx;

        this.random = new Random();

        //Initialize all the randomized AntiBan values!
        camera_radius_off_by_min = 5;
        camera_radius_off_by_max = 11; //FIXME: Randomize these values!



        //Randomize the values below a little
        //if(this.random.nextInt() == 0)
        //    camera_radius_off_by_min -= this.random.nextInt() + this.random.nextInt();



    }

    private static Npc getNearestBanker(ClientContext ctx) {
        return ctx.npcs.select().id(ID.ID_BANKERS).nearest().peek();
    }

    private static GameObject getNearestBooth(ClientContext ctx) {
        return ctx.objects.select().id(ID.ID_BANK_BOOTHS).nearest().peek();
    }

    //Opens bank
    public static void openBank(ClientContext ctx) {
        if(new Random().nextInt() == 0) //TODO: RANDOMIZE THIS CHANCE!
            getNearestBanker(ctx).interact("Bank");
        else
            getNearestBooth(ctx).interact("Bank");
    }

    public static Tile getTileToAggiesHouse() {
        return Tiles.AREA_AGGIES_HOUSE[new Random().nextInt(Tiles.AREA_AGGIES_HOUSE.length)];
    }

    public static Tile getTileToAggiesFrontYard(ClientContext ctx) {
        return Tiles.AREA_AGGIES_FRONTYARD[new Random().nextInt(Tiles.AREA_AGGIES_FRONTYARD.length)];
    }

    public static Tile getTileToBank(ClientContext ctx) {
        return Tiles.TILES_BANK[new Random().nextInt(Tiles.TILES_BANK.length)];
    }

    public static void turnCameraToBank(ClientContext ctx) {
        //Turn camera either to a random banker or a random bank booth.
        if(new Random().nextInt(2) == 0) {
            System.out.println("LOG: Turning to a banker.");
            ctx.camera.turnTo(getNearestBanker(ctx));
        }else {
            System.out.println("LOG: Turning to a bank booth.");
            ctx.camera.turnTo(getNearestBooth(ctx));
        }
    }


    public void turnToNorth() {
        //this.ctx.camera.angle('n');
        int angle = 0;

        angle += getAngleOffset();

        if(angle < 0)
            //angle = 360 - angle;
            angle += 360;

        Logger.log("[DEBUG] Turning camera to South (" + angle + " degrees)");
        this.ctx.camera.angle(angle);
    }

    public void turnToEast() {
        //this.ctx.camera.angle('e');
        int angle = 270;

        angle += getAngleOffset();

        Logger.log("[DEBUG] Turning camera to East (" + angle + " degrees)");
        this.ctx.camera.angle(angle);
    }

    public void turnToSouth() {
        //this.ctx.camera.angle('s');
        int angle = 180;

        angle += getAngleOffset();

        Logger.log("[DEBUG] Turning camera to South (" + angle + " degrees)");
        this.ctx.camera.angle(angle);
    }

    public void turnToWest() {
        //this.ctx.camera.angle('w');
        int angle = 90;

        angle += getAngleOffset();

        Logger.log("[DEBUG] Turning camera to West (" + angle + " degrees)");
        this.ctx.camera.angle(angle);
    }

    private int getAngleOffset() {
        if(this.random.nextInt(2) == 0) //negative offset
            return (-1 * (random.nextInt((this.camera_radius_off_by_max - this.camera_radius_off_by_min)) + this.camera_radius_off_by_min));
        else //positive offset
            return (random.nextInt((this.camera_radius_off_by_max - this.camera_radius_off_by_min)) + this.camera_radius_off_by_min);
    }

    //This is here to help open the door
    public static void setCameraPitchDown(ClientContext ctx) {
        Logger.log("AntiBank.setCameraPitchDown():");

        //Determines if we need to change the pitch
        int down_percentage;
        Random random = new Random();
        down_percentage = (random.nextInt(8) + 55);
        Logger.log("\tdown_percentage = " + down_percentage);

        Logger.log("\tCurrent camera pitch: " + ctx.camera.pitch());
        if(ctx.camera.pitch() > down_percentage) {
            Logger.log("Changing pitch to: " + down_percentage);
            ctx.camera.pitch(down_percentage);
        }else
            Logger.log("\t\tWe don't have to change the camera pitch.");
    }

    public static void setCameraPitchUp(ClientContext ctx) {
        Logger.log("AntiBank.setCameraPitchUp():");

        //Determines if we need to change the pitch
        int up_percentage;
        Random random = new Random();
        up_percentage = (random.nextInt(8) + 68);
        Logger.log("\tup_percentage = " + up_percentage);

        Logger.log("\tCurrent camera pitch: " + ctx.camera.pitch());
        if(ctx.camera.pitch() < up_percentage) {
            Logger.log("Changing pitch to: " + up_percentage);
            ctx.camera.pitch(up_percentage);
        }else
            Logger.log("\t\tWe don't have to change the camera pitch.");


    }

    //Returns random delay (ms) (small one that is added to the 'main' delay) which increases as the runtime increases (simulates human fatigue)
    /*public int getDelay() {
        //TODO
    }*/

}
