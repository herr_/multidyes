package API;

import org.powerbot.script.*;
import org.powerbot.script.rt6.ClientContext;
import org.powerbot.script.rt6.GameObject;
import org.powerbot.script.rt6.Player;
import resources.ID;

import java.util.Random;
import java.util.concurrent.Callable;

public class Door {

    private final static int id_opened = ID.ID_DOOR_OPENED;
    private final static int id_closed = ID.ID_DOOR_CLOSED;

    private final ClientContext ctx;
    private final Tile openDoorTile;
    private final Tile closedDoorTile;

    private int chance_interact_with_left_mouse;

    private Random random;


    public Door(ClientContext ctx, Tile openDoorTile, Tile closedDoorTile) {
        this.ctx = ctx;
        this.openDoorTile = openDoorTile;
        this.closedDoorTile = closedDoorTile;

        this.random = new Random();
        this.chance_interact_with_left_mouse = random.nextInt(5) + 7;
    }


    //Checks whether the door is in the viewport
    public boolean isDoorInViewPort() {
        return getDoor().inViewport();
    }


    public boolean isDoorOpen() {

        System.out.println("----------------------------------");
        System.out.println("isDoorOpen():");

        //If the open door is at the openDoorTile it's open then
        if(ctx.objects.select().at(this.openDoorTile).peek().id() == id_opened) {
            System.out.println("\tDoor id at the TILE_DOOR = " + id_opened);
            System.out.println("\treturn value: true");
            System.out.println("----------------------------------");
            return true;
        }

        System.out.println("\tDoor id at the TILE_DOOR = " + id_closed);
        System.out.println("\treturn value: false");
        System.out.println("----------------------------------");
        return false;
    }

    //NOTE: Door
    public void openDoor() {

        System.out.println("----------------------------------");
        System.out.println("openDoor():");

        //If door is already open, return.
        if(isDoorOpen())
            return;

        GameObject door = getDoor();

        //Set pitch down so we could interact with it easily
        AntiBan.setCameraPitchDown(ctx);

        ctx.camera.turnTo(door);

        //DBG
        System.out.println("DBG: closed door (" + door.toString() + " options:");
        for(String option : door.actions()) {
            System.out.println(option);
        }

        //Left click sometimes to open the door
        if(random.nextInt(this.chance_interact_with_left_mouse) == 0)
            door.click();
        else
            door.interact("Open");

        //Wait until the door is open.
        if(!isDoorOpen()) {
            final Player player = ctx.players.local();

            Condition.wait(new Callable<Boolean>() {
                @Override
                public Boolean call() {
                    return isDoorOpen();
                }
            }, 300, 5); //It isn't really 300ms since RSBot randomizes this value a little.
        }

        if(isDoorOpen())
            AntiBan.setCameraPitchUp(ctx);

        System.out.println("----------------------------------");
    }


    public void closeDoor() {
        if(isDoorOpen())
            getDoor().interact("Close");
    }


    public GameObject getDoor() {
        GameObject door;

        if(isDoorOpen())
            door = ctx.objects.select().at(this.openDoorTile).peek();
        else
            door = ctx.objects.select().at(this.closedDoorTile).peek();

        System.out.println("----------------------------------");
        System.out.println("getDoor():");
        System.out.println("\tDoor object: " + door.toString());
        System.out.println("\tDoor position: " + door.tile().toString());
        if(door.id() == id_opened)
            System.out.println("\tDoor id: " + id_opened + " (OPENED)");
        else
            System.out.println("\tDoor id: " + id_closed + " (CLOSED)");
        System.out.println("----------------------------------");

        return door;
    }

}