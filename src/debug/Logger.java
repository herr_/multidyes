package debug;

import org.powerbot.script.rt6.ClientContext;

public class Logger {

    //In the future we should log stuff to logfile (When this script is so ready that it doesn't need any of printing stuff)
    public static void log(String s) {
        System.out.println(s);
    }

    public static void fatal(String s, ClientContext ctx) {
        System.err.println(s);
        ctx.controller.stop();
    }

}
