package resources;

import org.powerbot.script.Tile;

public class Tiles {

    //Open door and closed door are on other tiles?
    public final static Tile TILE_DOOR_OPEN = new Tile(3087, 3259);
    public final static Tile TILE_DOOR_CLOSED = new Tile(3088, 3259);


    //NOTE: Use these only to walk to the bank!
    public final static Tile[] TILES_BANK = {
            new Tile(3089, 3245),
            new Tile(3089, 3246),
            new Tile(3090, 3246),
            new Tile(3092, 3245),
            new Tile(3092, 3246),
            new Tile(3093, 3245),
            new Tile(3093, 3246)
    };

    //These tiles are just for determining whether we're in the bank.
    public final static Tile[] AREA_BANK = {
            new Tile(3092, 3240),
            new Tile(3092, 3241),
            new Tile(3092, 3242),
            new Tile(3092, 3243),
            new Tile(3092, 3244),
            new Tile(3092, 3245),
            new Tile(3092, 3246),
            new Tile(3093, 3240),
            new Tile(3093, 3241),
            new Tile(3093, 3242),
            new Tile(3093, 3243),
            new Tile(3093, 3244),
            new Tile(3093, 3245),
            new Tile(3093, 3246)
    };

    //Aggie's house
    public final static Tile[] AREA_AGGIES_HOUSE = {
            new Tile(3081, 3259),
            new Tile(3081, 3260),
            new Tile(3081, 3261),
            new Tile(3082, 3258),
            new Tile(3082, 3259),
            new Tile(3082, 3260),
            new Tile(3082, 3261),
            new Tile(3083, 3259),
            new Tile(3083, 3261),
            new Tile(3084, 3258),
            new Tile(3084, 3259),
            new Tile(3084, 3260),
            new Tile(3084, 3261),
            new Tile(3085, 3258),
            new Tile(3085, 3259),
            new Tile(3085, 3260),
            new Tile(3085, 3261),
            new Tile(3086, 3258),
            new Tile(3086, 3259),
            new Tile(3086, 3260),
            new Tile(3086, 3261),
            new Tile(3086, 3262),
            new Tile(3087, 3259),
            new Tile(3087, 3260),
            new Tile(3087, 3261)
    };

    public final static Tile[] AREA_AGGIES_FRONTYARD = {
            new Tile(3088, 3259),
            new Tile(3089, 3258),
            new Tile(3089, 3259),
            new Tile(3089, 3260)
    };

}
